package com.mpg.itcane.template.util;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class CommonUtil {

	public static Timestamp getCurrentDateTimestamp() {
        Timestamp today = null;
        try {
            Date nowDate = Calendar.getInstance().getTime();
            today = new java.sql.Timestamp(nowDate.getTime());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return today;
    }

}
