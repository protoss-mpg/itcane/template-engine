package com.mpg.itcane.template.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mpg.itcane.template.model.ResponseModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class ApplicationInformationController {

	@GetMapping("/applicationInform")
    public ResponseEntity<ResponseModel> applicationInform() {
		ResponseModel response = new ResponseModel();
		response.setSuccess(true);
		response.setError_code(null);
		response.setMessage("Template of Spring Boot Engine");
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        
		return ResponseEntity.ok().headers(headers).body(response);
	}
}
