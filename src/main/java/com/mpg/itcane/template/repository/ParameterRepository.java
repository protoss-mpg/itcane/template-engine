package com.mpg.itcane.template.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mpg.itcane.template.entity.Parameter;

public interface ParameterRepository extends JpaSpecificationExecutor<Parameter>, JpaRepository<Parameter, Long> {
}
