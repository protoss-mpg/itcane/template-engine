package com.mpg.itcane.template.configuration.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import lombok.extern.slf4j.Slf4j;
import com.mpg.itcane.template.util.CommonUtil;

@Slf4j
@Component
@Aspect
public class LoggingAspect {

    @Pointcut("@annotation(com.mpg.itcane.template.configuration.aop.annotation.LogAccess)")
    public void logAccessMethod() {
    }

    @Around("logAccessMethod()")
    public Object profile(ProceedingJoinPoint pjp) throws Throwable {
            long start = System.currentTimeMillis();
            Object output = pjp.proceed();
            long elapsedTime = System.currentTimeMillis() - start;
            log.info(pjp.getSignature().getName()+"|"+CommonUtil.getCurrentDateTimestamp()+"|"+elapsedTime);
           
            return output;
    }

}

