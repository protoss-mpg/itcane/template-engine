package com.mpg.itcane.template;

public class ApplicationConstant {

	/* ERROR CODE */
	public static String ERROR_CODE_PROCESS_FAIL        = "EMIERR001";
	public static String ERROR_CODE_THROW_EXCEPTION     = "EMIERR002";
	public static String ERROR_CODE_NO_DATA_FOUND       = "EMIERR003";
	public static String ERROR_CODE_REQUIRE_DATA        = "EMIERR004";
}
