<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<groupId>com.mpg.itcane</groupId>
	<artifactId>templateEngine</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>templateEngine</name>
	<description>Template Service Engine</description>
	
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.1.8.RELEASE</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<java.version>1.8</java.version>
		<sonar.exclusions>**/com/mpg/itcane/template/*,
		                  **/com/mpg/itcane/template/configuration/aop/*,
		                  **/com/mpg/itcane/template/configuration/aop/annotation/*,
		                  **/com/mpg/itcane/template/configuration/swagger/*,
		                  **/com/mpg/itcane/template/entity/*,
		                  **/com/mpg/itcane/template/model/*,
		                  **/com/mpg/itcane/template/util/*</sonar.exclusions>
	</properties>

	<dependencies>
	    <!-- Web Layer -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter</artifactId>
		</dependency>
		
		<dependency>
	        <groupId>org.springframework.boot</groupId>
	        <artifactId>spring-boot-starter-web</artifactId>
	        <exclusions>
	            <exclusion>
	                <groupId>org.springframework.boot</groupId>
	                <artifactId>spring-boot-starter-logging</artifactId>
	            </exclusion>
	        </exclusions>
	    </dependency>
		
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-mail</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-log4j2</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-log4j12</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		
		<dependency>
			<groupId>com.protosstechnology</groupId>
			<artifactId>commons-util</artifactId>
			<version>1.0.1</version>
		</dependency>
		
		<!-- Persistence Layer -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>
		
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<optional>true</optional>
		</dependency>

		<dependency>
            <groupId>net.sf.flexjson</groupId>
            <artifactId>flexjson</artifactId>
            <version>2.1</version>
        </dependency>

        
        <!-- Database Layer -->
		<dependency>
                <groupId>com.microsoft.sqlserver</groupId>
                <artifactId>mssql-jdbc</artifactId>
                <version>6.1.0.jre8</version>
        </dependency>
        
        <!-- Java Mail API -->
        <dependency>
            <groupId>com.sun.mail</groupId>
            <artifactId>javax.mail</artifactId>
            <version>1.5.6</version>
        </dependency>
        
		<dependency>
		    <groupId>org.subethamail</groupId>
		    <artifactId>subethasmtp-wiser</artifactId>
		    <version>1.2</version>
		</dependency>

        <!-- Test Stack -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
		
		<dependency>
			<groupId>com.h2database</groupId>
			<artifactId>h2</artifactId>
            <scope>test</scope>
		</dependency>
		
		<!-- Tool -->
		 <dependency>
		   <groupId>io.springfox</groupId>
		   <artifactId>springfox-swagger-ui</artifactId>
		   <version>2.9.2</version>
		</dependency>
		<dependency>
		   <groupId>io.springfox</groupId>
		   <artifactId>springfox-swagger2</artifactId>
		   <version>2.9.2</version>
		</dependency>
		<dependency>
			<groupId>org.jacoco</groupId>
			<artifactId>jacoco-maven-plugin</artifactId>
			<version>0.8.4</version>
			<scope>provided</scope>
		</dependency>
        
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>0.8.4</version>
				<configuration>
					<excludes>
						<exclude>**/com/mpg/itcane/template/Application*.class</exclude>
						<exclude>**/com/mpg/itcane/template/Engine*.class</exclude>
						<exclude>**/com/mpg/itcane/template/configuration/aop/*.class</exclude>
						<exclude>**/com/mpg/itcane/template/configuration/aop/annotation/*.class</exclude>
						<exclude>**/com/mpg/itcane/template/configuration/swagger/*.class</exclude>
						<exclude>**/com/mpg/itcane/template/entity/*.class</exclude>
						<exclude>**/com/mpg/itcane/template/model/*.class</exclude>
						<exclude>**/com/mpg/itcane/template/util/*.class</exclude>
						<exclude>**/test/*.*</exclude>
					</excludes>
				</configuration>
				<executions>
					<execution>
						<id>default-prepare-agent</id>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
					</execution>
					<execution>
						<id>default-report</id>
						<phase>prepare-package</phase>
						<goals>
							<goal>report</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

</project>
